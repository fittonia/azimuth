use evdev_rs::enums::EV_KEY as EvKey;
use servo::keyboard_types::Key as ServoKey;
use super::parse_ev_key::parse_ev_key;
use super::servo_keys::SERVO_KEYS;
use super::servo_keys::servo_key_index;

use std::str::FromStr;

#[derive(Debug)]
enum Action {
	CharacterKey1(u8),
	CharacterKey2(u8),
	Key(u8),
	EnableState(u8),
	DisableState(u8),
	ToggleState(u8),
}

#[derive(Debug)]
struct KeyMapElement {
	ev_key: EvKey,
	mask: u8,
	condition: u8,
	action: Action,
}

#[derive(Debug)]
pub struct KeyMap {
	characters: Vec<String>,
	keys: Vec<KeyMapElement>,
	state: u8,
}

impl KeyMap {
	pub fn new(keymap_str: &str) -> Result<Self, (u32, &'static str)> {
		let mut characters = vec![];
		let mut keys = vec![];

		let mut rdr = csv::Reader::from_reader(keymap_str.as_bytes());
		let mut row = 2;
		for result in rdr.records() {
			let record = match result {
				Ok(result) if result.len() == 5 => Ok(result),
				_ => Err((row, "Bad row structure")),
			}?;
			let ev_key = match parse_ev_key(&record[0]){
				Some(key) => Ok(key),
				None => Err((row, "Bad ev_key")),
			}?;
			let mask = match record[1].parse::<u8>() {
				Ok(v) => Ok(v),
				Err(_) => Err((row, "Bad mask")),
			}?;
			let condition = match record[2].parse::<u8>() {
				Ok(v) => Ok(v),
				Err(_) => Err((row, "Bad condition")),
			}?;
			let action = match &record[3] {
				"char" => {
					let idx = characters.len();
					characters.push(String::from(&record[4]));
					if idx < 256 {
						Ok(Action::CharacterKey1(idx as u8))
					} else if idx < 512 {
						Ok(Action::CharacterKey2((idx - 256) as u8))
					} else {
						Err((row, "Two many char rows"))
					}
				},
				"key" => match ServoKey::from_str(&record[4]) {
					Ok(key) => match servo_key_index(key) {
						Some(idx) => Ok(Action::Key(idx)),
						None => Err((row, "Unknown key value")),
					},
					Err(_) => Err((row, "Invalid value")),
				},
				"enable" => match record[4].parse::<u8>() {
					Ok(v) if v < 6 => Ok(Action::EnableState(v)),
					_ => Err((row, "Invalid value")),
				},
				"disable" => match record[4].parse::<u8>() {
					Ok(v) if v < 6 => Ok(Action::DisableState(v)),
					_ => Err((row, "Invalid value")),
				},
				"toggle" => match record[4].parse::<u8>() {
					Ok(v) if v < 6 => Ok(Action::ToggleState(v)),
					_ => Err((row, "Invalid value")),
				},
				_ => Err((row, "Invalid action")),
			}?;
			keys.push(KeyMapElement {
				ev_key,
				mask,
				condition,
				action,
			});
			row += 1;
		}

		Ok(KeyMap {
			characters,
			keys,
			state: 0,
		})
	}

	pub fn translate(&mut self, ev_key: EvKey, down: bool) -> Option<ServoKey> {
		let mut state = ((down as u8) << 7) | self.state;
		for el in &self.keys {
			if el.ev_key == ev_key && (state & el.mask) == el.condition {
				return match el.action {
					Action::CharacterKey1(i) => {
						let s = &self.characters[i as usize];
						return Some(ServoKey::Character(s.clone()))
					},
					Action::CharacterKey2(i) => {
						let s = &self.characters[(i as usize) + 256];
						return Some(ServoKey::Character(s.clone()))
					},
					Action::Key(i) => return Some(SERVO_KEYS[i as usize].clone()),
					Action::EnableState(i) => {
						state |= 1 << i;
						self.state = state & 0x7f;
						None
					},
					Action::DisableState(i) => {
						state &= !(1 << i);
						self.state = state & 0x7f;
						None
					},
					Action::ToggleState(i) => {
						state ^= 1 << i;
						self.state = state & 0x7f;
						None
					},
				};
			}
		}
		None
	}
}