use evdev_rs::Device as EvDevice;
use evdev_rs::InputEvent as EvInputEvent;
use evdev_rs::enums::EventCode as EvCode;
use evdev_rs::enums::EV_KEY as EvKey;
use evdev_rs::enums::EV_REL as EvRel;
use evdev_rs::ReadFlag as EvReadFlag;
use std::fs::{File, read_dir};

use servo::keyboard_types::KeyState;
use servo::keyboard_types::KeyboardEvent;
use servo::keyboard_types::Code as KeyCode;
use servo::keyboard_types::Location as KeyLocation;
use servo::keyboard_types::Modifiers as KeyboardModifiers;
use servo::compositing::windowing::MouseWindowEvent;
use servo::compositing::windowing::WindowEvent;
use servo::script_traits::MouseButton;
use servo::script_traits::WheelDelta;
use servo::script_traits::WheelMode;
use servo::webrender_api::units::DevicePoint;

mod servo_keys;
mod parse_ev_key;
mod keymap;
use keymap::KeyMap;

pub struct InputDevices {
	event_queues: Vec<EvDevice>,
	mouse_pos: DevicePoint,
	mouse_speed: f32,
	mouse_updated: bool,
	keymap: KeyMap,
}

impl InputDevices {
	pub fn new(keymap_str: &str) -> Self {
		Self {
			event_queues: Vec::new(),
			mouse_pos: DevicePoint::new(10.0, 10.0),
			mouse_speed: 1.5,
			mouse_updated: true,
			keymap: KeyMap::new(keymap_str).unwrap(),
		}
	}

	pub fn refresh_inputs(&mut self) {
		self.event_queues = Vec::new();
		for dirent in read_dir("/dev/input/").unwrap() {
			let dirent = dirent.unwrap();
			let dirent_name = dirent.file_name().into_string().unwrap();
			if dirent_name.starts_with("event") {
				let fd = File::open(dirent.path()).unwrap();
				let mut d = EvDevice::new().unwrap();
				d.set_fd(fd).unwrap();
				self.event_queues.push(d);
			}
		}
	}

	pub fn get_input_events(&self) -> Vec<EvInputEvent> {
		let mut input_events = Vec::new();
		for eq in &self.event_queues {
			while eq.has_event_pending() {
				input_events.push(eq.next_event(EvReadFlag::NORMAL).unwrap().1);
			}
		}
		input_events
	}

	fn mapped_event(&mut self, event: &EvInputEvent) -> Option<WindowEvent> {
		match &event.event_code {
			EvCode::EV_KEY(key) => match self.keymap.translate(key.clone(), event.value > 0) {
				Some(servo_key) => Some(WindowEvent::Keyboard(KeyboardEvent {
					state: match event.value {
						0 => KeyState::Up,
						_ => KeyState::Down,
					},
					key: servo_key,
					code: KeyCode::Tab,
					location: KeyLocation::Standard,
					modifiers: KeyboardModifiers::empty(),
					repeat: event.value == 2,
					is_composing: true,
				})),
				_ => None,
			}
			_ => unreachable!()
		}
	}

	fn mouse_btn_event(&self, down: bool, btn: MouseButton, servo_events: &mut Vec<WindowEvent>) -> WindowEvent {
		WindowEvent::MouseWindowEventClass(if down {
			MouseWindowEvent::MouseDown(btn, self.mouse_pos)
		} else {
			let e = MouseWindowEvent::Click(btn, self.mouse_pos);
			servo_events.push(WindowEvent::MouseWindowEventClass(e));
			MouseWindowEvent::MouseUp(btn, self.mouse_pos)
		})
	}

	fn mouse_movement_relative(&mut self, value: f32, x: bool) -> WindowEvent {
		let axis = match x {
			true => &mut self.mouse_pos.x,
			_ => &mut self.mouse_pos.y,
		};
		*axis += value * self.mouse_speed;
		if *axis < 0.0 {
			*axis = 0.0;
		}
		self.mouse_updated = true;
		WindowEvent::Idle
	}

	pub fn input_events_for_servo(&mut self) -> Vec<WindowEvent> {
		let mut servo_events = vec![];
		for evdev_event in self.get_input_events() {
			let e = match &evdev_event.event_code {
				EvCode::EV_REL(EvRel::REL_X) => self.mouse_movement_relative(evdev_event.value as f32, true),
				EvCode::EV_REL(EvRel::REL_Y) => self.mouse_movement_relative(evdev_event.value as f32, false),
				EvCode::EV_REL(EvRel::REL_WHEEL) => {
					WindowEvent::Wheel(WheelDelta {
						x: 0.0,
						y: evdev_event.value as f64,
						z: 0.0,
						mode: WheelMode::DeltaLine,
					}, self.mouse_pos)
				},
				EvCode::EV_KEY(EvKey::BTN_LEFT) => {
					self.mouse_btn_event(evdev_event.value == 0, MouseButton::Left, &mut servo_events)
				},
				EvCode::EV_KEY(EvKey::BTN_RIGHT) => {
					self.mouse_btn_event(evdev_event.value == 0, MouseButton::Right, &mut servo_events)
				},
				EvCode::EV_KEY(_) => match self.mapped_event(&evdev_event) {
					Some(keyboard_event) => keyboard_event,
					None => WindowEvent::Idle,
				},
				_ => WindowEvent::Idle,
			};
			if !matches!(e, WindowEvent::Idle) {
				servo_events.push(e);
			}
		};
		if self.mouse_updated {
			servo_events.push(WindowEvent::MouseWindowMoveEventClass(self.mouse_pos));
			self.mouse_updated = false;
		}
		servo_events
	}
}