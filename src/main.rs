extern crate euclid;
extern crate drm;
extern crate gbm;

use servo::Servo;
use servo::BrowserId;
use servo::compositing::windowing::WindowEvent;
use servo::servo_url::ServoUrl;
use servo::servo_config::opts;
use servo::embedder_traits::resources as servo_rsc;
use servo::embedder_traits::EmbedderMsg;

use servo::graphics_traits::ContextAttributeFlags;
use servo::graphics_traits::GraphicsContext;

use std::env;
use std::fs::read_to_string;
use getopts::Options;

mod input;
use input::InputDevices;

mod output;
use output::EglWindow;
use output::EglGpu;

mod resources;
use resources::ResourceReader;

fn main() {
	servo_rsc::set(Box::new(ResourceReader));

	let args: Vec<String> = env::args().collect();
	let options = Options::new();

	// todo: replace with argument parsing:
	let url = env::var_os("AZ_URL")
		.expect("AZ_URL not defined, pls do.")
		.into_string().unwrap();

	let keymap = read_to_string(
		env::var_os("AZ_KEYMAP")
			.expect("AZ_KEYMAP not defined, pls do.")
			.into_string()
			.unwrap()
	).expect("Could not read keymap at AZ_KEYMAP");

	opts::from_cmdline_args(options, &args);

	let gfx_ctx = EglGpu::new("azimuth-main", true, ContextAttributeFlags::all(), None).unwrap();
	let egl_window = EglWindow(gfx_ctx);

	let mut servo = Servo::new::<EglGpu>(egl_window, None);
	let mut input_devices = InputDevices::new(&keymap);
	input_devices.refresh_inputs();

	let browser_id = BrowserId::new();
	let url = ServoUrl::parse(&url).unwrap();
	let mut init_events = vec![WindowEvent::NewBrowser(url, browser_id)];
	init_events.push(WindowEvent::SelectBrowser(browser_id));
	servo.handle_events(init_events);

	let mut shutdown = false;
	
	while !shutdown {
		let mut to_servo = input_devices.input_events_for_servo();

		let from_servo = servo.get_events();
		for e in &from_servo {
			if let EmbedderMsg::AllowNavigationRequest(p, _) = e.1 {
				to_servo.push(WindowEvent::AllowNavigationResponse(p, true));
			}
			match &e.1 {
				EmbedderMsg::ChangePageTitle(Some(title)) => {
					println!("NEW TITLE: {:?}", title);
				},
				EmbedderMsg::Status(Some(status)) => {
					println!("NEW STATUS: {:?}", status);
				},
				EmbedderMsg::Shutdown => {
					shutdown = true;
				},
				_ => println!("New servo event: {:?}", e.1),
			}
		}

		servo.handle_events(to_servo);
	}
}