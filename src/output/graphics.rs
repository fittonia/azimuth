use servo::graphics_traits::GraphicsContext;
use servo::graphics_traits::ContextAttributeFlags;
use servo::graphics_traits::Error;
use servo::graphics_traits::TextureKind;
use servo::graphics_traits::SurfaceInfo;
use servo_media::player::context::{GlApi, GlContext, NativeDisplay};
use euclid::default::{Size2D, Point2D};
use khronos_egl as egl;

use egl::Context as EglContext;
use egl::Display as EglDisplay;
use egl::Surface as EglSurface;

use drm::Device as DrmDevice;
use drm::control::Device as ControlDevice;
use drm::control::framebuffer::Info as DrmFrameBuffer;
use drm::control::connector;
use drm::control::ResourceInfo;
use drm::control::crtc;
use drm::control::Mode;
use drm::control::framebuffer;

use gbm::Surface as GBMSurface;
use gbm::Format as GBMFormat;
use gbm::SurfaceBufferHandle as GbmSurfaceBufferHandle;
use gbm::BufferObjectFlags as GBMFlags;
use gbm::Device as GBMDevice;
use gbm::AsRaw;

use std::fs::{OpenOptions, File, read_dir};
use std::os::unix::io::{AsRawFd, RawFd};
use std::ffi::c_void;
use std::ops::Deref;
use std::env;

use std::time::Instant;

struct DrmCard(File);
impl DrmDevice for DrmCard { }
impl ControlDevice for DrmCard { }
impl AsRawFd for DrmCard {
	fn as_raw_fd(&self) -> RawFd { self.0.as_raw_fd() }
}

// struct DisplayProps {
// 
// }
// 
// struct TextureProps {
// 
// }
// 
// enum Output {
// 	Display(DisplayProps),
// 	Texture(TextureProps),
// }

pub struct EglGpu {
	pub name: String,
	pub wsurf: bool,
	pub flags: ContextAttributeFlags,
	pub size: Size2D<u32>,
	gbm_device: GBMDevice<DrmCard>,
	egl_context: EglContext,
	egl_display: EglDisplay,
	gbm_surface: GBMSurface<DrmFrameBuffer>,
	egl_surface: EglSurface,
	last_buffer_object: Option<GbmSurfaceBufferHandle<DrmFrameBuffer>>,

	drm_connector: connector::Handle,
	drm_mode: Mode,
	drm_crtc: crtc::Handle,

	frames: u32,
	timer: Instant,
}

const EGL: egl::Instance<egl::Static> = egl::Instance::new(egl::Static);

impl EglGpu {
	fn get_framebuffer_from_buffer_object(&self, buffer_handle: &mut GbmSurfaceBufferHandle<DrmFrameBuffer>) -> DrmFrameBuffer {
		if let Some(fb) = buffer_handle.userdata().unwrap() {
			*fb
		} else {
			let fb = framebuffer::create(&self.gbm_device, buffer_handle.deref().deref()).unwrap();
			buffer_handle.set_userdata(fb).unwrap();
			fb
		}
	}

	fn set_crtc(&self, fb: &DrmFrameBuffer){
		let frame = crtc::set(
			&self.gbm_device,
			self.drm_crtc,
			fb.handle(),
			&[ self.drm_connector ],
			(0, 0),
			Some(self.drm_mode),
		);

		if let Err(e) = frame {
			println!("could not present surface: {:?}", e);
		}
	}
}

impl GraphicsContext for EglGpu {
	fn new(name: &str, wsurf: bool, flags: ContextAttributeFlags, size: Option<Size2D<u32>>) -> Result<Self, Error> {
		let (card_idx, connector_idx, mode_idx, crtc_idx) = get_az_gpu();

		// SIGSEGV when static linking failed
		let gbm_device = GBMDevice::new(DrmCard({
			let mut options = OpenOptions::new();
			options.read(true);
			options.write(true);
			options.open(format!("/dev/dri/card{:?}", card_idx)).unwrap()
		})).unwrap();

		let _output = if wsurf {
			assert!(size == None, "wsurf && size != None")
		} else {
			unimplemented!()
		};

		let res_handles = gbm_device.resource_handles().unwrap();
		let drm_crtc = res_handles.crtcs()[crtc_idx];
		let drm_connector = res_handles.connectors()[connector_idx];
		let info = gbm_device.resource_info::<connector::Info>(drm_connector).unwrap();
		let drm_mode = info.modes()[mode_idx];

		let size = drm_mode.size();
		let size = Size2D::new(size.0 as u32, size.1 as u32);

		let egl_display = EGL.get_display(gbm_device.as_raw() as *mut c_void).unwrap();
		EGL.initialize(egl_display).unwrap();
		EGL.bind_api(egl::OPENGL_ES_API).unwrap();

		let egl_config = {
			let alpha_size = (flags.alpha   as i32) << 3;
			let depth_size = (flags.depth   as i32) << 3;
			let stenc_size = (flags.stencil as i32) << 3;

			let config_attributes = [
				egl::SURFACE_TYPE, egl::WINDOW_BIT,
				egl::RENDERABLE_TYPE, egl::OPENGL_ES2_BIT,
				egl::RED_SIZE,     8,
				egl::GREEN_SIZE,   8,
				egl::BLUE_SIZE,    8,
				egl::ALPHA_SIZE,   alpha_size,
				egl::DEPTH_SIZE,   depth_size,
				egl::STENCIL_SIZE, stenc_size,
				egl::NONE,
			];
			EGL.choose_first_config(egl_display, &config_attributes).unwrap().unwrap()
		};

		let egl_context = {
			let context_attributes = [
				egl::CONTEXT_MAJOR_VERSION, 3, // should be a parameter in the future
				egl::CONTEXT_MINOR_VERSION, 0,
				egl::NONE,
			];
			EGL.create_context(egl_display, egl_config, None, &context_attributes).unwrap()
		};

		let gbm_surface = gbm_device.create_surface::<DrmFrameBuffer>(
			size.width, size.height,
			GBMFormat::ARGB8888,
			GBMFlags::SCANOUT | GBMFlags::RENDERING,
		).unwrap();

		let egl_surface = {
			let gbm_surface_raw = gbm_surface.as_raw() as *mut c_void;
			let surface_attributes = [
				egl::NONE as usize
			];
			EGL.create_platform_window_surface(
				egl_display,
				egl_config,
				gbm_surface_raw,
				&surface_attributes
			).unwrap()
		};

		Ok(EglGpu {
			name: String::from(name),
			wsurf,
			flags,
			size,
			gbm_device,
			egl_context,
			egl_display,
			gbm_surface,
			egl_surface,
			last_buffer_object: None,

			drm_connector,
			drm_mode,
			drm_crtc,

			frames: 0,
			timer: Instant::now(),
		})
	}

	fn gl_version(&self) -> (u8, u8) {
		let d = self.egl_display;
		let c = self.egl_context;
		let major = EGL.query_context(d, c, egl::CONTEXT_MAJOR_VERSION);
		let minor = EGL.query_context(d, c, egl::CONTEXT_MINOR_VERSION);
		(major.unwrap() as u8, minor.unwrap() as u8)
	}

	fn make_current(&self) -> Result<(), Error> {
		let s = Some(self.egl_surface);
		EGL.make_current(self.egl_display, s, s, Some(self.egl_context)).unwrap();
		Ok(())
	}

	fn surface_info(&self) -> Result<Option<SurfaceInfo>, Error> {
		// DIFFERENT IF !WSURF
		Ok(Some(SurfaceInfo {
			size: self.size,
			id: self.gbm_surface.as_raw() as usize,
			context_id: self.egl_context.as_ptr() as usize,
			framebuffer_object: 0,
		}))
	}

	fn resize(&mut self, _size: Size2D<u32>) -> Result<(), Error> {
		unimplemented!()
	}

	fn present(&mut self, _cursor_pos: &Point2D<u32>) -> Result<(), Error> {
		self.make_current()?;
		if let Err(_) = EGL.swap_buffers(self.egl_display, self.egl_surface) {
			return Err(Error);
		}

		let mut buf = unsafe {
			self.gbm_surface.lock_front_buffer().unwrap()
		};
		let fb = self.get_framebuffer_from_buffer_object(&mut buf);
		self.set_crtc(&fb);

		let mut last_bo = Some(buf);
		std::mem::swap(&mut self.last_buffer_object, &mut last_bo);
		if let Some(last_bo) = last_bo {
			std::mem::drop(last_bo);
		}

		self.frames += 1;
		if self.frames == 10 {
			let duration = self.timer.elapsed().as_secs_f32();
			println!("fps: {:?}", 10.0 / duration);
			self.timer = Instant::now();
			self.frames = 0;
		}

		Ok(())
	}

	fn get_proc_address(&self, name: &str) -> *const c_void {
		EGL.get_proc_address(name).map_or(std::ptr::null(), |p| p as *const _)
	}

	fn create_surface_texture(&mut self) -> Result<u32, Error> {
		unimplemented!()
	}

	fn destroy_surface_texture(&mut self, _surface_texture: u32) -> Result<(), Error> {
		unimplemented!()
	}

	fn texture_kind(&self) -> TextureKind {
		TextureKind::TwoDimensions
	}

	fn get_gl_context(&self) -> GlContext {
		GlContext::Egl(self.egl_context.as_ptr() as usize)
	}

	fn get_native_display(&self) -> NativeDisplay {
		NativeDisplay::Egl(self.egl_display.as_ptr() as usize)
	}

	fn get_gl_api(&self) -> GlApi {
		GlApi::Gles2
	}
}

fn get_az_gpu() -> (usize, usize, usize, usize) {
	let (config_idx, mut writer) = match env::var_os("AZ_GPU") {
		Some(osstr) => {
			let az_gpu = osstr.into_string().unwrap();
			(az_gpu.parse::<usize>().unwrap(), None)
		},
		None => {
			let mut writer = csv::Writer::from_path("available_gpu_configs.csv").unwrap();
			writer.write_record(&["AZ_GPU", "GPU", "connector", "resolution", "refresh-rate", "CRTC"]).unwrap();
			(0, Some(writer))
		}
	};
	let mut config = None;
	let mut iter_idx = 0;
	for dirent in read_dir("/dev/dri/").unwrap() {
		let dirent = dirent.unwrap();
		let dirent_name = dirent.file_name().into_string().unwrap();
		if dirent_name.starts_with("card") {
			let card_idx = &dirent_name[4..];
			let drm_device = DrmCard({
				let mut options = OpenOptions::new();
				options.read(true);
				options.write(true);
				options.open(dirent.path()).unwrap()
			});
			let res_handles = drm_device.resource_handles().unwrap();
			let mut connector_idx = 0;
			for drm_connector in res_handles.connectors() {
				let info = drm_device.resource_info::<connector::Info>(*drm_connector).unwrap();
				let mut mode_idx = 0;
				for drm_mode in info.modes() {
					let name = drm_mode.name().to_str().unwrap();
					let mut crtc_idx = 0;
					for _drm_crtc in res_handles.crtcs() {
						if let Some(ref mut writer) = writer {
							writer.write_record(&[
								iter_idx.to_string(),
								card_idx.to_string(),
								format!("{:?}", info.connector_type()),
								name.to_string(),
								drm_mode.vrefresh().to_string(),
								crtc_idx.to_string(),
							]).unwrap();
						}
						if iter_idx == config_idx {
							config = Some((
								card_idx.parse::<usize>().unwrap(),
								connector_idx,
								mode_idx,
								crtc_idx
							));
							if let None = writer {
								return config.unwrap();
							}
						}
						iter_idx += 1;
						crtc_idx += 1;
					}
					mode_idx += 1;
				}
				connector_idx += 1;
			}
		}
	}
	if let Some(ref mut writer) = writer {
		writer.flush().unwrap();
	}
	config.unwrap()
}