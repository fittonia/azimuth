use servo::compositing::windowing::EmbedderCoordinates;
use servo::compositing::windowing::AnimationState;
use servo::compositing::windowing::WindowMethods;
use servo::webrender_api::units::DeviceIntSize;
use servo::webrender_api::units::DeviceIntRect;
use servo::euclid::Scale;
use servo::euclid::Point2D;

mod graphics;
pub use graphics::EglGpu;

pub struct EglWindow(pub EglGpu);

impl WindowMethods for EglWindow {
	fn get_coordinates(&self) -> EmbedderCoordinates {
		let size = self.0.size;
		let size = DeviceIntSize::new(size.width as i32, size.height as i32);
		EmbedderCoordinates {
			hidpi_factor: Scale::new(1.0),
			screen: size,
			screen_avail: size,
			window: (size, Point2D::zero()),
			framebuffer: size,
			viewport: DeviceIntRect::new(Point2D::zero(), size),
		}
	}

	/// no-op
	fn set_animation_state(&self, _state: AnimationState) {}

    fn ctx(&mut self) -> &mut Self {
    	self
    }
}

use servo::graphics_traits::GraphicsContext;
use servo::graphics_traits::ContextAttributeFlags;
use servo::graphics_traits::Error;
use servo::graphics_traits::TextureKind;
use servo::graphics_traits::SurfaceInfo;
use servo_media::player::context::{GlApi, GlContext, NativeDisplay};
use euclid::default::{Size2D, Point2D as UknPoint2D};
use std::ffi::c_void;

impl GraphicsContext for EglWindow {
	fn new(_: &str, _: bool, _: ContextAttributeFlags, _: Option<Size2D<u32>>) -> Result<Self, Error> {
		unimplemented!()
	}

	fn gl_version(&self) -> (u8, u8) {
		self.0.gl_version()
	}

	fn make_current(&self) -> Result<(), Error> {
		self.0.make_current()
	}

	fn surface_info(&self) -> Result<Option<SurfaceInfo>, Error> {
		self.0.surface_info()
	}

	fn resize(&mut self, size: Size2D<u32>) -> Result<(), Error> {
		self.0.resize(size)
	}

	fn present(&mut self, cursor_pos: &UknPoint2D<u32>) -> Result<(), Error> {
		self.0.present(cursor_pos)
	}

	fn get_proc_address(&self, name: &str) -> *const c_void {
		self.0.get_proc_address(name)
	}

	fn create_surface_texture(&mut self) -> Result<u32, Error> {
		self.0.create_surface_texture()
	}
	fn destroy_surface_texture(&mut self, surface_texture: u32) -> Result<(), Error> {
		self.0.destroy_surface_texture(surface_texture)
	}
	fn texture_kind(&self) -> TextureKind {
		self.0.texture_kind()
	}

	fn get_gl_context(&self) -> GlContext {
		self.0.get_gl_context()
	}
	fn get_native_display(&self) -> NativeDisplay {
		self.0.get_native_display()
	}
	fn get_gl_api(&self) -> GlApi {
		self.0.get_gl_api()
	}
}