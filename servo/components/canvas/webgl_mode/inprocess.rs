/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::webgl_thread::{WebGLThread, WebGLThreadInit/*, WebXRBridgeInit*/};
use canvas_traits::webgl::webgl_channel;
use canvas_traits::webgl::{WebGLMsg, WebGLThreads};
use euclid::default::Size2D;
use gleam;
use sparkle::gl::GlType;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use graphics_traits::GraphicsContext;
use webrender_traits::{
    WebrenderExternalImageApi, WebrenderExternalImageRegistry, WebrenderImageSource,
};
use webrender::render_api as wr_render_api;

pub struct WebGLComm {
    pub webgl_threads: WebGLThreads,
    pub image_handler: Box<dyn WebrenderExternalImageApi>,
    pub output_handler: Option<Box<dyn webrender_api::ExternalImageHandler>>,
    // pub webxr_layer_grand_manager: WebXRLayerGrandManager<WebXRSurfman>,
}

impl WebGLComm {
    /// Creates a new `WebGLComm` object.
    pub fn new<GfxCtx: GraphicsContext>(
        _webrender_gl: Rc<dyn gleam::gl::Gl>,
        webrender_api_sender: wr_render_api::RenderApiSender,
        webrender_doc: webrender_api::DocumentId,
        external_images: Arc<Mutex<WebrenderExternalImageRegistry>>,
        api_type: GlType,
    ) -> WebGLComm {
        debug!("WebGLThreads::new()");
        let (sender, receiver) = webgl_channel::<WebGLMsg>().unwrap();
        /*let webrender_swap_chains = SwapChains::new();*/
        // let webxr_init = WebXRBridgeInit::new(sender.clone());
        // let webxr_layer_grand_manager = webxr_init.layer_grand_manager();

        // This implementation creates a single `WebGLThread` for all the pipelines.
        let init = WebGLThreadInit {
            webrender_api_sender,
            webrender_doc,
            external_images,
            sender: sender.clone(),
            receiver,
            /*webrender_swap_chains: webrender_swap_chains.clone(),*/
            api_type,
            // webxr_init,
        };

        let external = WebGLExternalImages;

        WebGLThread::<GfxCtx>::run_on_own_thread(init);

        WebGLComm {
            webgl_threads: WebGLThreads(sender),
            image_handler: Box::new(external),
            output_handler: None,//output_handler.map(|b| b as Box<_>),
            // webxr_layer_grand_manager: webxr_layer_grand_manager,
        }
    }
}

/// Bridge between the webrender::ExternalImage callbacks and the WebGLThreads.
struct WebGLExternalImages;

impl WebrenderExternalImageApi for WebGLExternalImages {
    fn lock(&mut self, _id: u64) -> (WebrenderImageSource, Size2D<i32>) {
        unimplemented!()
    }

    fn unlock(&mut self, _id: u64) {
        unimplemented!()
    }
}