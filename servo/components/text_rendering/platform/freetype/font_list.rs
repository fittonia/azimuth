/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::fs::read_dir;
use std::env;

pub fn for_each_available_family<F>(mut callback: F)
where
    F: FnMut(String),
{
    callback(String::from("PublicSans"));
}

pub fn for_each_variation<F>(family_name: &str, mut callback: F)
where
    F: FnMut(String),
{
	let font_dir = env::var_os("AZ_FONTS").unwrap().into_string().unwrap() + family_name;
	let paths = read_dir(&font_dir);
    if let Ok(paths) = paths {
    	for path in paths {
	    	let path = path.unwrap().path();
	    	callback(String::from(path.to_str().unwrap()));
	    }
    }
}

pub fn system_default_family(_generic_name: &str) -> Option<String> {
    None
}

pub static SANS_SERIF_FONT_FAMILY: &'static str = "PublicSans";

// Based on gfxPlatformGtk::GetCommonFallbackFonts() in Gecko
pub fn fallback_font_families(_codepoint: Option<char>) -> Vec<&'static str> {
    vec!["PublicSans"]
}
