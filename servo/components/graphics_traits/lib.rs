/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#![deny(unsafe_code)]

use euclid::default::{Size2D, Point2D};
use std::os::raw::c_void;
use servo_media::player::context::{GlApi, GlContext, NativeDisplay};

#[derive(Debug, Clone)]
pub struct Error;

#[derive(Debug, Clone)]
pub struct ContextAttributeFlags {
    pub alpha: bool,
    pub depth: bool,
    pub stencil: bool,
}

impl ContextAttributeFlags {
    pub fn empty() -> Self {
        ContextAttributeFlags {
            alpha: false,
            depth: false,
            stencil: false,
        }
    }
    pub fn all() -> Self {
        ContextAttributeFlags {
            alpha: true,
            depth: true,
            stencil: true,
        }
    }
}

pub struct SurfaceInfo {
    pub size: Size2D<u32>,
    pub id: usize,
    pub context_id: usize,
    pub framebuffer_object: u32,
}

pub enum TextureKind {
    Rectangle,
    TwoDimensions,
    Other,
}

pub trait GraphicsContext: Sized {
    /// Creates a new instance, associated to a context and a surface.
    /// The buffer must have stencil, depth, and alpha channels.
    fn new(
        name: &str,
        window_surface: bool,
        flags: ContextAttributeFlags,
        size: Option<Size2D<u32>>,
    ) -> Result<Self, Error>;

    fn gl_version(&self) -> (u8, u8);
    fn make_current(&self) -> Result<(), Error>;
    fn surface_info(&self) -> Result<Option<SurfaceInfo>, Error>;
    fn resize(&mut self, size: Size2D<u32>) -> Result<(), Error>;
    /// This method must not be called if window_surface was false in new()
    fn present(&mut self, cursor_pos: &Point2D<u32>) -> Result<(), Error>;

    fn get_proc_address(&self, name: &str) -> *const c_void;

    fn create_surface_texture(&mut self) -> Result<u32, Error>;
    fn destroy_surface_texture(&mut self, surface_texture: u32) -> Result<(), Error>;
    fn texture_kind(&self) -> TextureKind;

    fn get_gl_context(&self) -> GlContext;
    fn get_native_display(&self) -> NativeDisplay;
    fn get_gl_api(&self) -> GlApi;
}
